import {Component, Input} from '@angular/core';
import {Card, CardDek} from "../../lib/CardDeck";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})


export class CardComponent  {
  cardDek = new CardDek();
  @Input() rank = this.cardDek.getCard().rank;
  @Input() suit = this.cardDek.getCard().suit
  @Input() denomination = 0;
  @Input() symbol = '';
  card = new Card(this.suit, this.rank);

  getSymbol() {
    if (this.suit === 'diams') {
       this.symbol = '♦';
    }if (this.suit === 'hearts') {
       this.symbol = '♥';
    }if (this.suit === 'clubs') {
       this.symbol =  '♣';
    }if (this.suit === 'spades') {
       this.symbol = '♠';
    }
    return this.symbol
  }

  getClassname(){

    return 'card rank-' + this.rank.toLowerCase() + ' ' + this.suit;
  }
  addCard(){

  }
}

