

export class Card  {

  constructor(public suit: string, public rank: string) {
  }

  getScore(){
      if (this.rank === '2') {
        this.rank = '2';
      }if (this.rank === '3') {
        this.rank = '3';
      }if (this.rank === '4') {
        this.rank =  '4';
      }if (this.rank === '5') {
        this.rank = '5';
      }if (this.rank === '6') {
        this.rank = '6';
      }if (this.rank === '7') {
        this.rank = '7';
      }if (this.rank === '8') {
        this.rank =  '8';
      }if (this.rank === '9') {
        this.rank = '9';
      }if (this.rank === '10') {
        this.rank = '10';
      }if (this.rank === 'J' || this.rank === 'Q' || this.rank === 'K') {
        this.rank = '10';
      }if (this.rank === 'A') {
        this.rank =  '11';
      }
      return this.rank
  }
}

export class CardDek  {
  constructor(public massCard: Card[] = []) {
    const suit: string[] = ['diams', 'hearts', 'clubs', 'spades'];
    const rank: string[] = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    for (let string of rank) {
      for (const stringTwo of suit) {
        const card = new Card(stringTwo, string);
        this.massCard.push(card)
      }
    }
  }

  getCard(): Card {
    const randomCard = this.massCard[Math.floor(Math.random() * this.massCard.length)];
    const cardSplice: Card[] = [randomCard].splice(0, 1);
    return cardSplice[0];
  }

    getCards(howMany:number): Card[]{
    const massive = [];
      for (let i = 0; i < howMany; i++) {
        massive.push(this.getCard())
      }
        return massive
    }
}
